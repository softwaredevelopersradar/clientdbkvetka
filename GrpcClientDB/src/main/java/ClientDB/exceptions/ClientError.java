package ClientDB.exceptions;

import java.util.Map;

import static java.util.Map.entry;

public class ClientError {
    public static String getDefenition(EnumClientError error)
    {
        try { return dicError.get(error); }
        catch (Exception ex) { return ""; }
    }

    private static final Map<EnumClientError, String> dicError = Map.ofEntries(
        entry( EnumClientError.UnknownError, "Error: "),
        entry( EnumClientError.NoConnection, "Error: No Connection! "),
        entry( EnumClientError.IncorrectEndpoint, "Error: Endpoint is incorrect!")
    );
}
