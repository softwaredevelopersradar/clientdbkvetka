package ClientDB.exceptions;

public class ExceptionClient extends Exception
{
    public EnumClientError error;

    public ExceptionClient(EnumClientError error)
    {
        super(ClientError.getDefenition(error));
        this.error = error;
    }

    public ExceptionClient(String message)
    {
        super(message);
        error = EnumClientError.UnknownError;
    }

    public ExceptionClient(ExceptionClient exception)
    {
        super(exception.getMessage());
        error = exception.error;
    }
}

