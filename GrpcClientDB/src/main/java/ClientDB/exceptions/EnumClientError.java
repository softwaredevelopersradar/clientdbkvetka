package ClientDB.exceptions;

public enum EnumClientError {
    None,
    UnknownError,
    IncorrectEndpoint,
    NoConnection,
}
