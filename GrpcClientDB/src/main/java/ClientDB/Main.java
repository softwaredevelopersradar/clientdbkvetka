package ClientDB;


import ClientDB.eventargs.TableEventArgs;
import ClientDB.exceptions.ExceptionClient;
import ClientDB.grpc_service.AbonentResFHSSMessage;
import ClientDB.grpc_service.AnalogResFFMessage;
import KvetkaModels.*;

import java.util.*;
import java.util.function.Consumer;

public class Main{

    private static int id;
    private static String user = "Polina Test";
    private static String serverIp = "localhost";
    private static int serverPort = 30051;

    public static void main(String[] args) {
//        System.out.println("Yes");
////        // Access a service running on the local machine on port 50051
////        String target = "localhost:30051";
////        System.out.println("This app name: " + user);
////        System.out.println("Trying to connect server: " + target);
////
        try {
            ClientDB client = new ClientDB(user, serverIp, serverPort ); //Подключиться

            client.connect();
            client.ping(user);


            //TODO: потом уберу, просто для примера подписки на ивенты
           client.tables.get(NameTable.TableJammer).onUpdatedTable.addListener((Consumer<TableEventArgs<StationsModel>>)((x) -> test3(x)));
            client.tables.get(NameTable.TableRoute).onUpdatedTable.addListener((Consumer<TableEventArgs<StationsModel>>)((x) -> test3(x)));
            client.tables.get(NameTable.TableGlobalProperties).onUpdatedTable.addListener((Consumer<TableEventArgs<GlobalPropertiesModel>>)((x) -> test3(x)));
            client.tables.get(NameTable.TableResFF).onUpdatedTable.addListener((Consumer<TableEventArgs<ReconFWSModel>>)((x) -> test3(x)));
//            client.tables.get(NameTable.TableFreqForbidden).onUpdatedTable.addListener((Consumer<TableEventArgs<FreqsForbiddenModel>>)((x) -> test3(x)));
//            client.tables.get(NameTable.TableResFFJam).onUpdatedTable.addListener((Consumer<TableEventArgs<SuppressFWSModel>>)((x) -> test3(x)));
//            client.tables.get(NameTable.TableResFHSSJam).onUpdatedTable.addListener((Consumer<TableEventArgs<SuppressFHSSModel>>)((x) -> test3(x)));
//            client.tables.get(NameTable.TempSuppressFF).onUpdatedTable.addListener((Consumer<TableEventArgs<TempSuppressFWSModel>>)((x) -> test3(x)));
//            client.tables.get(NameTable.TableResFHSS).onUpdatedTable.addListener((Consumer<TableEventArgs<FHSSModel>>)((x) -> test3(x)));
//            client.tables.get(NameTable.TableSubscriberResFHSS).onUpdatedTable.addListener((Consumer<TableEventArgs<SubscriberFHSSModel>>)((x) -> test3(x)));
//            client.tables.get(NameTable.TableTrackSubscriberResFHSS).onUpdatedTable.addListener((Consumer<TableEventArgs<TrackSubscriberFHSSModel>>)((x) -> test3(x)));
//            client.tables.get(NameTable.TableAnalogResFF).onUpdatedTable.addListener((Consumer<TableEventArgs<AnalogReconFWSModel>>)((x) -> test3(x)));
            //            Consumer<AbstractCommonTable> addHandler = (x) -> testAdd(x);
//            client.tables.get(NameTable.TableJammer).onAddRecord.addListener(addHandler);


//          client.tables.get(NameTable.TableJammer).tableEvents.addListener((x)->test(x.getType()));

//            var testClient = new TestClient();
//            client.clientEvents.addListener(testClient);
//            client.tables.get(NameTable.TableJammer).tableEvents.addListener(testClient);
//            client.tables.get(NameTable.TableADSB).tableEvents.addListener(testClient);


            var record = new StationsModel(0, new Coord(24,23,20),"loh", Role.Master, "empty", false, 23, CommunicationType.Router3G_4G  );
 //           client.tables.get(NameTable.TableJammer).add(record);
               
               //            try {
//            }
//            catch (Exception ex)
//            {
//                System.err.println(ex.getMessage());
//            }


            final Random random = new Random();
            var tempsup = new TempSuppressModel(random.nextInt(), Led.Blue, Led.Green, Led.Red, 1);
            var tempsup2 = tempsup.toTempSuppressFWSModel();
//            client.tables.get(NameTable.TempSuppressFF).add(tempsup2);

            
            var subFhss = new SubscriberFHSSModel(0,0, new Date(2021,10,2, 10,5,0), new TrackSubscriberFHSSModel(0,0,0,new Coord((double) 2,(double) 3,(float)4), (float) 10,(float)20,(float)200,(float)3000));
          //  var fhss = new FHSSModel(0, random.nextDouble(), random.nextDouble(), (float)20, (byte)1, (short)20, (short)20, (short)20, Arrays.asList(subFhss));
//            client.tables.get(NameTable.TableResFHSS).add(fhss);
            
            var fws = new ReconFWSModel(); fws.setFrequency(random.nextInt()); fws.setSignalFlag(SignalFlag.Analog); fws.setControl(Led.Blue);
            client.tables.get(NameTable.TableResFF).add(fws);

            var fwsD = new DigitalReconFWSModel(); fwsD.setFrequency(random.nextInt()); fwsD.setSignalClass(DigitalSignalClass.SC_ALE3G); fwsD.setRadioNetwork("my"); fwsD.setSubscriberInitiator("my"); fwsD.setSubscriberRecipient("my");
//            client.tables.get(NameTable.TableDigitalResFF).add(fwsD);
            var fwsA = new AnalogReconFWSModel(); fwsA.setFrequency(random.nextInt()); fwsA.setTypeSignal("AM"); fwsA.setBandDemodulation(8000); 
//            fwsA.setReconFWS(fws);
            //client.tables.get(NameTable.TableAnalogResFF).add(fwsA);
            var fwsDistr = new ReconFWSDistribModel(); fwsDistr.setFrequency(random.nextInt());
           // client.tables.get(NameTable.TableResFFDistribution).add(fwsDistr);
            var point = new RoutePointModel(0,0, (short)10, new Coord((double) 2,(double) 3,(float)4), random.nextDouble());
 //           var route = new RouteModel(0, (short)3, "3", random.nextDouble(), new Date(2021,10,2, 10,5,0), Arrays.asList(point));
            //           var listRoute = client.tables.get(NameTable.TableRoute).load();
 //           var lastRoute = listRoute.get(listRoute.size()-1);
           
           // client.tables.get(NameTable.TableRoute).change(lastRoute);

            var global = new GlobalPropertiesModel(); global.setFhssFftResolution(FftResolution.N32768); global.setJsgAmPower(PowerPercent.P100);
            // client.tables.get(NameTable.TableGlobalProperties).add(global);
            //client.tables.get(NameTable.TableRoute).add(route);
            
//            var freq = new SpecFreqsModel(false, 0, random.nextFloat(), random.nextFloat(), "", 2);
//            var freq2 = freq.toFreqsForbiddenModel();
//            client.tables.get(NameTable.TableFreqForbidden).add(freq);
////
//            var supFWS = new SuppressFWSModel(0, true, (byte)0,random.nextDouble(),random.nextFloat(), new InterferenceParams(), new Coord(), 0, (byte)0, (byte)0, "String.Em", 2);
//            client.tables.get(NameTable.TableResFFJam).add(supFWS);
//
//            var supFHSS = new SuppressFHSSModel(0, true, (byte)0,random.nextDouble(), random.nextDouble(), new InterferenceParams(), 0, new Byte[]{4,5,6}, (byte)0, "String.Em", 2);
//            client.tables.get(NameTable.TableResFHSSJam).add(supFHSS);
//            var listFreq = client.tables.get(NameTable.TableFreqForbidden).load();
//            var listFreqFilter = ((IDependentAsp)client.tables.get(NameTable.TableFreqForbidden)).loadByFilter(1);
//
//            ((IDependentAsp)client.tables.get(NameTable.TableFreqForbidden)).clearByFilter(2);
//            var airplane = new AirplanesModel("RA65733", new Coord(random.nextFloat(),random.nextFloat(),random.nextFloat()), new Date(), (byte)1, "Russia");
//            client.tables.get(NameTable.TableADSB).add(airplane);
//
//
//            //client.disconnect();
//
//
//
            Object o = new Object();
            synchronized (o) {
                o.wait();
            }
//
//
        } catch (InterruptedException | ExceptionClient e) {
            e.printStackTrace();
        } finally {
//            // ManagedChannels use resources like threads and TCP connections. To prevent leaking these
//            // resources the channel should be shut down when it will no longer be used. If it may be used
//            // again leave it running.
//            channel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
        }
    }

//    private static void test(Class<?> test)
//    {
//        System.out.println("get update for " + test.getAnnotation(InfoTable.class).name());
//    }
//
//    private static void test2(Class<?> test)
//    {
//        System.out.println("get update for " + test.getAnnotation(InfoTable.class).name());
//    }
//
    private static void test3(TableEventArgs<?> test)
    {
        System.out.println("get update for " + test.getType().getAnnotation(InfoTable.class).name());
    }
//
//    private static void testAdd(AbstractCommonTable record)
//    {
//        System.out.println("Added to " + record.getClass());
//    }
}
