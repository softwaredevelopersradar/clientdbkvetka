package ClientDB.eventargs;

import ClientDB.exceptions.ClientError;
import ClientDB.exceptions.EnumClientError;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public class ConnectionEventArgs {
    public enum actServer
    {
        Connect,
        Disconnect
    }

    private EnumClientError currentError;

    private String getMessage;

    public final Map<String, String> dicMess = Map.ofEntries(
            Map.entry(actServer.Connect.toString(), "Connected"),
            Map.entry(actServer.Disconnect.toString(), "Disconnected")
    );

    public ConnectionEventArgs(EnumClientError error)
    {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        getMessage = LocalDateTime.now().format(dtf) + " ";
        currentError = error;
        getMessage += ClientError.getDefenition(currentError) + " ";
    }

    public ConnectionEventArgs(EnumClientError error, String errorMess)
    {
        this(error); //TODO: проверить
        getMessage += errorMess;
    }

    public ConnectionEventArgs(actServer actServer)
    {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        getMessage = LocalDateTime.now().format(dtf) + " ";
        currentError = EnumClientError.None;
        getMessage += "";// DicMess[actServer.ToString()];
    }
    public ConnectionEventArgs(actServer actServer, String mess)
    {
        this(actServer); //TODO: проверить
        getMessage += mess;
    }



    public EnumClientError getCurrentError(){
        return this.currentError;
    }

    public String getGetMessage(){
        return this.getMessage;
    }
}
