package ClientDB.eventargs;

import KvetkaModels.*;

import java.util.ArrayList;

public class TableEventArgs<T extends AbstractCommonTable> {

    private ArrayList<T> table;
    private final Class<T> type;
    

    public TableEventArgs(ClassDataCommon lData, Class<T> clazz)
    {
        table = lData.toList(lData, clazz);
        type = clazz;
    }

//    public TableEventArgs(ArrayList<T> lData)
//    {
//        table = new ArrayList<>(lData);
//    }

    public ArrayList<T> getTable(){
        return this.table;
    }
    public Class<T> getType(){
        return this.type;
    }
}
