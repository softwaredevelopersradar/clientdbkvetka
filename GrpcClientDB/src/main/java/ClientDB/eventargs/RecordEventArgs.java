package ClientDB.eventargs;

import KvetkaModels.*;

public class RecordEventArgs {
    private AbstractCommonTable abstractRecord;

    private NameTable nameTable;

    private NameChangeOperation nameAction;


    public RecordEventArgs(NameTable nameTable, AbstractCommonTable record, NameChangeOperation action)
    {
        this.nameTable = nameTable;
        this.abstractRecord = record;
        this.nameAction = action;
    }

    public AbstractCommonTable getAbstractRecord() {
        return abstractRecord;
    }

    public NameTable getNameTable() {
        return nameTable;
    }

    public NameChangeOperation getNameAction() {
        return nameAction;
    }
}
