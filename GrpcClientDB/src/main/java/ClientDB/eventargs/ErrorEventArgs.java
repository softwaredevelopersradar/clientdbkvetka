package ClientDB.eventargs;

import KvetkaModels.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ErrorEventArgs {

    private NameTableOperation operation;

    private NameTable tableName;

    private String message;

    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");

    public ErrorEventArgs(NameTable table, NameTableOperation act, String message)
    {
        var time = LocalDateTime.now().format(dtf);
        this.message = time + "  ";
        this.message += table + " error: " + message;
        this.tableName = table;
        this.operation = NameTableOperation.None;
    }

    public ErrorEventArgs(String message)
    {
        var time = LocalDateTime.now().format(dtf);
        this.message = time + "  " + message;
    }

    public NameTableOperation getOperation(){ return this.operation;}
    public NameTable getTableName(){
        return this.tableName;
    }
    public String getMessage(){
        return this.message;
    }

}
