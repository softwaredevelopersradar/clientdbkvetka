package ClientDB;
import ClientDB.grpc_service.AnalogSignalType;
import ClientDB.grpc_service.Led;
import ClientDB.grpc_service.SignalFlag;
import com.google.protobuf.Int32Value;
import KvetkaModels.*;
import ClientDB.grpc_service.*;
import com.google.protobuf.InvalidProtocolBufferException;

import java.time.*;
import java.util.ArrayList;
import java.util.Date;



public class TypesConverter {

    private static Date convertToDate(com.google.protobuf.Timestamp time)
    {
        Instant now = Instant.ofEpochSecond(time.getSeconds(), time.getNanos());
        LocalDateTime local = LocalDateTime.ofInstant(now,  ZoneId.of("UTC"));
        Instant instant = local.toInstant(OffsetDateTime.now().getOffset());
        return Date.from(instant);
    }

    private static com.google.protobuf.Timestamp convertToTimestamp(Date time)
    {
        Instant now = time.toInstant();
        ZonedDateTime zdt = ZonedDateTime.ofInstant(now, ZoneId.systemDefault());
        LocalDateTime temp = zdt.toLocalDateTime();
        Instant instant =  temp.toInstant(ZoneOffset.UTC);
        return com.google.protobuf.Timestamp.newBuilder().setSeconds(instant.getEpochSecond())
                .setNanos(instant.getNano()).build();
    }
    

    public static KvetkaModels.NameTable convertToTableModels(EnumTables args) {
        return KvetkaModels.NameTable.values()[args.ordinal()];
    }

    public static EnumTables convertToProto(KvetkaModels.NameTable args) {
        return EnumTables.values()[args.ordinal()];
    }


    public static KvetkaModels.NameChangeOperation convertToTableModels(ChangeRecordOperation args) {
        return KvetkaModels.NameChangeOperation.values()[args.ordinal()];
    }
    public static ChangeRecordOperation convertToProto(KvetkaModels.NameChangeOperation args) {
        return ChangeRecordOperation.values()[args.ordinal()];
    }

    
    public static KvetkaModels.Led convertToTableModels(Led args) {
        return KvetkaModels.Led.values()[args.ordinal()];
    }
    public static Led convertToProto(KvetkaModels.Led args) {
        return Led.values()[args.ordinal()];
    }

    public static KvetkaModels.CommunicationType convertToTableModels(EnumCommunicationType args) {
        return KvetkaModels.CommunicationType.values()[args.ordinal()];
    }
    public static EnumCommunicationType convertToProto(KvetkaModels.CommunicationType args) {
        return EnumCommunicationType.values()[args.ordinal()];
    }

    public static KvetkaModels.DigitalSignalClass convertToTableModels(DigitalSignalType args) {
        return KvetkaModels.DigitalSignalClass.values()[args.ordinal()];
    }
    public static DigitalSignalType convertToProto(KvetkaModels.DigitalSignalClass args) {
        return DigitalSignalType.values()[args.ordinal()];
    }

    public static KvetkaModels.AnalogSignalType convertToTableModels(AnalogSignalType args) {
        return KvetkaModels.AnalogSignalType.values()[args.ordinal()];
    }
    public static AnalogSignalType convertToProto(KvetkaModels.AnalogSignalType args) {
        return AnalogSignalType.values()[args.ordinal()];
    }

    public static KvetkaModels.SignalFlag convertToTableModels(SignalFlag args) {
        return KvetkaModels.SignalFlag.values()[args.ordinal()];
    }
    public static SignalFlag convertToProto(KvetkaModels.SignalFlag args) {
        return SignalFlag.values()[args.ordinal()];
    }

    public static KvetkaModels.FftResolution convertToTableModels(EnumFftResolution args) {
        int num = args.getNumber();
        return KvetkaModels.FftResolution.values()[num-2];
    }
    public static EnumFftResolution convertToProto(KvetkaModels.FftResolution args) {
        int n = args.ordinal();
//        int num = args.getFftResolutionType();
//        EnumFftResolution res = EnumFftResolution.values()[num];
        return EnumFftResolution.values()[n+1];
    }

    public static KvetkaModels.PowerPercent convertToTableModels(EnumPowerPercent args) {
        return KvetkaModels.PowerPercent.values()[args.ordinal()];
    }
    public static EnumPowerPercent convertToProto(KvetkaModels.PowerPercent args) {
        return EnumPowerPercent.values()[args.ordinal()];
    }

    public static KvetkaModels.VereskType convertToTableModels(EnumVereskState args) {
        return KvetkaModels.VereskType.values()[args.ordinal()];
    }
    public static EnumVereskState convertToProto(KvetkaModels.VereskType args) {
        return EnumVereskState.values()[args.ordinal()];
    }

    public static KvetkaModels.ModeSound convertToTableModels(ResFFDistributMessage.EnumMode args) {
        return KvetkaModels.ModeSound.values()[args.ordinal()];
    }
    public static ResFFDistributMessage.EnumMode convertToProto(KvetkaModels.ModeSound args) {
        return ResFFDistributMessage.EnumMode.values()[args.ordinal()];
    }


    public static KvetkaModels.StationsModel convertToTableModels(JammerMessage args) {
        return new StationsModel(args.getId(), convertToTableModels(args.getCoordinates()), args.getCallsign(), KvetkaModels.Role.values()[args.getRole().ordinal()], args.getNote(), args.getIsOwn(), args.getAntennaAngle(), convertToTableModels(args.getCommunicationType()));
    }

    public static JammerMessage convertToProto(KvetkaModels.StationsModel args){
        return JammerMessage.newBuilder().setId(args.getId()).setCoordinates(convertToProto(args.getCoordinates())).setCallsign(args.getCallsign()).setRole(EnumRole.values()[args.getRole().ordinal()]).setNote(args.getNote()).setIsOwn(args.getIsOwn()).setAntennaAngle(args.getAntennaAngle()).setCommunicationType(convertToProto(args.getCommunicationType())).build();
    }


    public static KvetkaModels.Coord convertToTableModels(CoordMessage args){
        return new Coord(args.getLatitude(), args.getLongitude(), args.getAltitude());
    }

    public static CoordMessage convertToProto(KvetkaModels.Coord args){
        return CoordMessage.newBuilder().setLatitude(args.latitude).setLongitude(args.longitude).setAltitude(args.altitude).build();
    }

    public static KvetkaModels.InterferenceParams convertToTableModels(InterferenceParamsMessage args)
    {
        return new KvetkaModels.InterferenceParams((byte)args.getModulation(), (byte)args.getDeviation(), (byte)args.getManipulation(), (byte)args.getDuration());
    }
    public static InterferenceParamsMessage convertToProto(KvetkaModels.InterferenceParams args)
    {
        return InterferenceParamsMessage.newBuilder().setModulation(args.modulation).setDeviation(args.deviation).setManipulation(args.manipulation).setDuration(args.duration).build();
    }


    public static KvetkaModels.SpecFreqsModel convertToTableModels(FreqRangeMessage args){
        return new KvetkaModels.SpecFreqsModel(args.getIsActive(), args.getId() , args.getFmin(), args.getFmax(), args.getNote(), args.getJammerId());
    }

    public static FreqRangeMessage convertToProto(KvetkaModels.SpecFreqsModel args)
    {
        return FreqRangeMessage.newBuilder().setId(args.getId()).setIsActive(args.getIsCheck()).setFmin(args.getFreqMin()).setFmax(args.getFreqMax()).setNote(args.getNote()).setJammerId(args.getStationId()).build();
    }


    public static KvetkaModels.SectorsModel convertToTableModels(SectorMessage args){
        return new KvetkaModels.SectorsModel(args.getIsActive(), args.getId() , args.getAmin(), args.getAmax(), args.getNote(), args.getJammerId());
    }

    public static SectorMessage convertToProto(KvetkaModels.SectorsModel args){
        return SectorMessage.newBuilder().setId(args.getId()).setIsActive(args.getIsCheck()).setAmin(args.getAngleMin()).setAmax(args.getAngleMax()).setNote(args.getNote()).setJammerId(args.getStationId()).build();
    }


    public static KvetkaModels.AirplanesModel convertToTableModels(AirplaneMessage args){
        return new KvetkaModels.AirplanesModel(args.getICAO(), convertToTableModels(args.getCoordinates()), convertToDate(args.getTime()), (byte)args.getType(), args.getCountry(), args.getId());
    }

    public static AirplaneMessage convertToProto(KvetkaModels.AirplanesModel args){
        return AirplaneMessage.newBuilder().setICAO(args.getIcao())
                .setId(args.getId())
                .setCoordinates(convertToProto(args.getCoordinates()))
                .setTime(convertToTimestamp(args.getTime()))
                .setType(args.getType())
                .setCountry(args.getCountry()).build();
    }


//    public static KvetkaModels.ReconFWSModel convertToTableModels(ResFFJamMessage args){
//        return new KvetkaModels.SuppressFWSModel(args.getId(), true, args.getSign(), args.getFrequencyKhz(), args.get);
//    }
//
//    public static SectorMessage convertToProto(KvetkaModels.SuppressFWSModel args){
//        return SectorMessage.newBuilder().setId(args.getId()).setIsActive(args.getIsCheck()).setAmin(args.getAngleMin()).setAmax(args.getAngleMax()).setNote(args.getNote()).setJammerId(args.getStationId()).build();
//    }


    public static KvetkaModels.GlobalPropertiesModel convertToTableModels(GlobalSettingsMessage args) {
        return new GlobalPropertiesModel(args.getId(), convertToTableModels(args.getGnss()), args.getTimeJamming(),
                args.getNumberOfPhasesAveragings(), args.getNumberOfBearingAveragings(), args.getCourseAngle(), args.getAdaptiveThresholdFhss(),
                args.getNumberOfResInLetter(), args.getNumberOfChannelsInLetter(), args.getLongWorkingSignalDurationMs(),
                (byte)args.getPriority(), args.getThreshold(), args.getTimeRadiationFF(), args.getIsTest(),
                convertToTableModels(args.getFhssFftResolution()), args.getTimeRadiationFHSS(),
                args.getSearchSector(), args.getSearchTime(), args.getJsgAmPollInterval(), convertToTableModels(args.getJsgAmPower()));
    }

    public static GlobalSettingsMessage convertToProto(KvetkaModels.GlobalPropertiesModel args){
        return GlobalSettingsMessage.newBuilder().setId(args.getId()).setGnss(convertToProto(args.getGnss()))
                .setTimeJamming(args.getTimeJamming())
                .setNumberOfPhasesAveragings(args.getNumberOfPhasesAveragings()).setNumberOfBearingAveragings(args.getNumberOfBearingAveragings()).setCourseAngle(args.getCourseAngle()).setAdaptiveThresholdFhss(args.getAdaptiveThresholdFhss())
                .setNumberOfResInLetter(args.getNumberOfResInLetter()).setNumberOfChannelsInLetter(args.getNumberOfChannelsInLetter()).setLongWorkingSignalDurationMs(args.getLongWorkingSignalDurationMs())
                .setPriority(args.getPriority()).setThreshold(args.getThreshold()).setTimeRadiationFF(args.getTimeRadiationFF()).setIsTest(args.getIsTest())
                .setFhssFftResolution(convertToProto(args.getFhssFftResolution())).setTimeRadiationFHSS(args.getTimeRadiationFHSS())
                .setSearchSector(args.getSearchSector()).setSearchTime(args.getSearchTime())
                .setJsgAmPollInterval(args.getJsgAmPollInterval()).setJsgAmPower(convertToProto(args.getJsgAmPower())).build();
    }


    public static KvetkaModels.SuppressFWSModel convertToTableModels(ResFFJamMessage args){
        return new KvetkaModels.SuppressFWSModel(args.getId(), 
                args.getIsActive(), 
                (byte)args.getSign(), 
                args.getFrequencyKhz(), 
                args.getBearing(), 
                convertToTableModels(args.getInterferenceParams()), 
                convertToTableModels(args.getCoordinates()), 
                (short)args.getThreshold(), 
                (byte)args.getLetter(), 
                (byte)args.getPriority(),
                args.getJammerId());
    }
    
    public static ResFFJamMessage convertToProto(KvetkaModels.SuppressFWSModel args){
        return ResFFJamMessage.newBuilder().setId(args.getId())
                .setJammerId(args.getStationId())
                .setIsActive(args.getIsCheck())
                .setSign(args.getSign())
                .setFrequencyKhz(args.getFrequency())
                .setBearing(args.getBearing())
                .setInterferenceParams(convertToProto(args.getInterferenceParams()))
                .setThreshold(args.getThreshold())
                .setLetter(args.getLetter())
                .setPriority(args.getPriority())
                .setCoordinates(convertToProto(args.getCoordinates())).build();
    }

    
    public static KvetkaModels.SuppressFHSSModel convertToTableModels(ResFHSSJamMessage args){
        
        byte[] lettersbyte = args.getLetters().toByteArray();
        Byte[] lettersByte = new Byte[lettersbyte.length];
        for(int i = 0; i < lettersbyte.length; i++) { lettersByte[i] = lettersbyte[i]; }
        
        return new KvetkaModels.SuppressFHSSModel(args.getId(),
                args.getIsActive(),
                (byte)args.getSign(),
                args.getFrequencyKhzMin(),
                args.getFrequencyKhzMax(),
                convertToTableModels(args.getInterferenceParams()),
                (short)args.getThreshold(), 
                lettersByte,
                (byte)args.getPriority(),
                args.getJammerId());
    }
    
    public static ResFHSSJamMessage convertToProto(KvetkaModels.SuppressFHSSModel args){
        Byte[] lettersByte = args.getLetters();
        byte[] lettersbyte = new byte[lettersByte.length];
        for(int i = 0; i < lettersByte.length; i++) { lettersbyte[i] = lettersByte[i]; }
        
        return ResFHSSJamMessage.newBuilder().setId(args.getId())
                .setJammerId(args.getStationId())
                .setIsActive(args.getIsCheck())
                .setSign(args.getSign())
                .setFrequencyKhzMin(args.getFreqMin())
                .setFrequencyKhzMax(args.getFreqMax())
                .setInterferenceParams(convertToProto(args.getInterferenceParams()))
                .setThreshold(args.getThreshold())
                .setLetters(com.google.protobuf.ByteString.copyFrom(lettersbyte))
                .setPriority(args.getPriority()).build();
    }

    
    public static KvetkaModels.TempSuppressModel convertToTableModels(TempSuppressMessage args){
        return new KvetkaModels.TempSuppressModel(args.getId() , convertToTableModels(args.getControl()), convertToTableModels(args.getSuppress()), convertToTableModels(args.getRadiation()), args.getJammerId());
    }

    public static TempSuppressMessage convertToProto(KvetkaModels.TempSuppressModel args)
    {
        return TempSuppressMessage.newBuilder().setId(args.getId()).setControl(convertToProto(args.getControl())).setSuppress(convertToProto(args.getSuppress())).setRadiation(convertToProto(args.getRadiation())).setJammerId(args.getStationId()).build();
    }

    
    public static KvetkaModels.FHSSModel convertToTableModels(ResFHSSMessage args){
        var subscribersList = new ArrayList<SubscriberFHSSModel>();
        for (var t : args.getListSubscribersList())
        {
            subscribersList.add(convertToTableModels(t));
        }
        
        return new KvetkaModels.FHSSModel(args.getId(),
                args.getFrequencyKhzMin(),
                args.getFrequencyKhzMax(),
                args.getBandKhz(),
                (byte)args.getModulation(),
                (short)args.getVManipulation(),
                (short)args.getStepKHz(),
                (short)args.getImpulseDuration(),
                subscribersList,
                args.getIdBearingDb()
        );
    }

    public static ResFHSSMessage convertToProto(KvetkaModels.FHSSModel args){
        var result = ResFHSSMessage.newBuilder().setId(args.getId())
                .setFrequencyKhzMin(args.getFreqMin())
                .setFrequencyKhzMax(args.getFreqMax())
                .setBandKhz(args.getBand())
                .setModulation(args.getModulation())
                .setVManipulation(args.getManipulationVelocity())
                .setStepKHz(args.getStepKHz())
                .setImpulseDuration(args.getImpulseDuration())
                .setIdBearingDb(args.getIdBearingDb());

        var tempList = new ArrayList<AbonentResFHSSMessage>();
        for (var t : args.getListSubscribers())
        {
            tempList.add(convertToProto(t));
        }
        
        result.addAllListSubscribers(tempList);
        return result.build();
    }


    public static KvetkaModels.SubscriberFHSSModel convertToTableModels(AbonentResFHSSMessage args){
        return new KvetkaModels.SubscriberFHSSModel(args.getId(), args.getResFHSSId(), convertToDate(args.getTime()), convertToTableModels(args.getPosition()));
    }

    public static AbonentResFHSSMessage convertToProto(KvetkaModels.SubscriberFHSSModel args){
        
        return AbonentResFHSSMessage.newBuilder().setId(args.getId())
                .setResFHSSId(args.getFhssId())
                .setTime(convertToTimestamp(args.getTime()))
                .setPosition(convertToProto(args.getPosition())).build();
    }


    public static KvetkaModels.TrackSubscriberFHSSModel convertToTableModels(TrackAbonentResFHSSMessage args){
        return new KvetkaModels.TrackSubscriberFHSSModel(args.getId(),
                args.getAbonentResFHSSId(),
                args.getResFHSSId(),
                convertToTableModels(args.getCoordinates()),
                args.getBearingOwn(),
                args.getBearingLinked(),
                args.getDistanceOwn(),
                args.getDistanceLinked());
    }

    public static TrackAbonentResFHSSMessage convertToProto(KvetkaModels.TrackSubscriberFHSSModel args){
       return TrackAbonentResFHSSMessage.newBuilder().setId(args.getId())
                .setAbonentResFHSSId(args.getSubscriberFhssId())
                .setResFHSSId(args.getFhssId())
                .setCoordinates(convertToProto(args.getCoordinates()))
                .setBearingOwn(args.getBearingOwn())
                .setBearingLinked(args.getBearingLinked())
                .setDistanceOwn(args.getDistanceOwn())
                .setDistanceLinked(args.getDistanceLinked()).build();
    }


    public static KvetkaModels.ReconFWSModel convertToTableModels(ResFFMessage args){
        return new KvetkaModels.ReconFWSModel(args.getId(),
                convertToTableModels(args.getControl()),
                convertToTableModels(args.getSignalFlag()),
                args.getFrequencyKhz(),
                args.getBearingOwn(),
                args.getBearingLinked(),
                args.getLevel(),
                convertToTableModels(args.getCoordinates()),
                convertToDate(args.getTime()),
                args.getDistanceOwn(),
                args.getIsActive(),
                args.getAnalogResFFId() == com.google.protobuf.Int32Value.getDefaultInstance() ? null : args.getAnalogResFFId().getValue(),
                args.getDigitalResFFId() == com.google.protobuf.Int32Value.getDefaultInstance() ? null : args.getDigitalResFFId().getValue(),
                (short)args.getQuality());
    }

    public static ResFFMessage convertToProto(KvetkaModels.ReconFWSModel args){
        var result = ResFFMessage.newBuilder().setId(args.getId())
                .setSignalFlag(convertToProto(args.getSignalFlag()))
                .setFrequencyKhz(args.getFrequency())
                .setBearingOwn(args.getBearing1())
                .setBearingLinked(args.getBearing2())
                .setDistanceOwn(args.getDistanceOwn())
                .setLevel(args.getLevel())
                .setCoordinates(convertToProto(args.getCoordinates()))
                .setTime(convertToTimestamp(args.getTime()))
                .setIsActive(args.isIsCheck())
                .setControl(convertToProto(args.getControl()))
                .setQuality(args.getQuality());
        if(args.getAnalogReconFwsId() != null) {
            result.setAnalogResFFId(Int32Value.of(args.getAnalogReconFwsId()));
        }
        if(args.getDigitalReconFwsId() != null) {
            result.setDigitalResFFId(Int32Value.of(args.getDigitalReconFwsId()));
        }
        return result.build();
//        builder.setFoo(Int32Value.of(1234));
//        Int32Value.newBuilder().setValue(123).build()
    }

    
    public static KvetkaModels.DigitalReconFWSModel convertToTableModels(DigitalResFFMessage args){
        var tableResFF = args.getResFF() != null ? convertToTableModels(args.getResFF()) : null;
        return new KvetkaModels.DigitalReconFWSModel(args.getId(),
                convertToDate(args.getTimeBegin()),
                convertToDate(args.getTimeEnd()),
                args.getFrequencyKhz(),
                convertToTableModels(args.getSignalClass()),
                args.getBearingOwn(),
                args.getSubscriberInitiator(),
                args.getSubscriberRecipient(),
                args.getRadioNetwork(),
                args.getIsActive(),
                args.getHasMetadata(),
                convertToTableModels(args.getCoordinates()),
                tableResFF,
                convertToTableModels(args.getVereskState()),
                args.getVereskDbSessionId());
    }

    public static DigitalResFFMessage convertToProto(KvetkaModels.DigitalReconFWSModel args){
        var tableResFF = args.getReconFWS() != null ? convertToProto(args.getReconFWS()) : null;
//        int size = ByteBuffer.wrap(args.getVereskDbSessionId()).getInt();
//        long actualSize = Integer.toUnsignedLong();
        var result =DigitalResFFMessage.newBuilder().setId(args.getId())
                .setTimeBegin(convertToTimestamp(args.getTimeBegin()))
                .setTimeEnd(convertToTimestamp(args.getTimeEnd()))
                .setFrequencyKhz(args.getFrequency())
                .setSignalClass(convertToProto(args.getSignalClass()))
                .setBearingOwn(args.getBearingOwn())
                .setSubscriberInitiator(args.getSubscriberInitiator())
                .setSubscriberRecipient(args.getSubscriberRecipient())
                .setRadioNetwork(args.getRadioNetwork())
                .setCoordinates(convertToProto(args.getCoordinates()))
                .setIsActive(args.getIsCheck())
                .setHasMetadata(args.getHasMetadata())
                .setVereskState(convertToProto(args.getVereskType()))
                .setVereskDbSessionId(args.getVereskDbSessionId());
        if (tableResFF!=null) result.setResFF(tableResFF);
        return result.build();
    }

    
    public static KvetkaModels.AnalogReconFWSModel convertToTableModels(AnalogResFFMessage args){
        var tableResFF = args.hasResFF() ? convertToTableModels(args.getResFF()) : null;
        return new KvetkaModels.AnalogReconFWSModel(args.getId(),
                args.getFrequencyKhz(),
                args.getBandKhz(),
                args.getBearingOwn(),
                args.getLevel(),
                args.getSignalType(),
                args.getBandDemudulation(),
                convertToTableModels(args.getCoordinates()),
                convertToDate(args.getTime()),
                args.getIsActive(),
                args.getBandAudio(),
                tableResFF);
    }

    public static AnalogResFFMessage convertToProto(KvetkaModels.AnalogReconFWSModel args){
        var tableResFF = args.getReconFWS() != null ? convertToProto(args.getReconFWS()) : null;
        var result = AnalogResFFMessage.newBuilder().setId(args.getId())
                .setFrequencyKhz(args.getFrequency())
                .setBandKhz(args.getBand())
                .setBearingOwn(args.getBearing1())
                .setCoordinates(convertToProto(args.getCoordinates()))
                .setLevel(args.getLevel())
                .setSignalType(args.getTypeSignal())
                .setBandDemudulation(args.getBandDemodulation())
                .setTime(convertToTimestamp(args.getTime()))
                .setIsActive(args.isIsCheck())
                .setBandAudio(args.getBandAudio());
        if (tableResFF!=null) result.setResFF(tableResFF);
        return result.build();
    }


    public static KvetkaModels.ReconFWSDistribModel convertToTableModels(ResFFDistributMessage args){
       return new KvetkaModels.ReconFWSDistribModel(args.getId(),
                args.getFrequencyKhz(),
                args.getBandKhz(),
                args.getBearingOwn(),
                args.getBearingLinked(),
                args.getLevel(),
                convertToTableModels(args.getCoordinates()),
                convertToDate(args.getTime()),
                args.getDistanceOwn(),
                args.getIsActive(),
                args.getIsRecording(),
               args.getDemodulation(),
               args.getBandwidthKHz(),
               args.getSampleRate(),
               convertToTableModels(args.getMode()),
               args.getAgcLevel(),
               args.getMgcCoefficient()
               );
    }
    
    public static ResFFDistributMessage convertToProto(KvetkaModels.ReconFWSDistribModel args){
        return ResFFDistributMessage.newBuilder().setId(args.getId())
                .setFrequencyKhz(args.getFrequency())
                .setBandKhz(args.getBand())
                .setBearingOwn(args.getBearing())
                .setBearingLinked(args.getBearingLinked())
                .setDistanceOwn(args.getDistanceOwn())
                .setLevel(args.getLevel())
                .setCoordinates(convertToProto(args.getCoordinates()))
                .setTime(convertToTimestamp(args.getTime()))
                .setIsActive(args.getIsCheck())
                .setIsRecording(args.getIsRecording())
                .setDemodulation(args.getDemodulation())
                .setBandwidthKHz(args.getBandwidthKHz())
                .setSampleRate(args.getSampleRate())
                .setMode(convertToProto(args.getMode()))
                .setAgcLevel(args.getAgcLevel())
                .setMgcCoefficient(args.getMgcCoefficient()).build();
    }

    
    public static KvetkaModels.RouteModel convertToTableModels(RouteMessage args){
        var subscribersList = new ArrayList<RoutePointModel>();
        for (var t : args.getListPointsList())
        {
            subscribersList.add(convertToTableModels(t));
        }

        return new KvetkaModels.RouteModel(args.getId(),
                (short)args.getNumRoute(),
                args.getName(),
                args.getDistanceOwn(),
                convertToDate(args.getTime()),
                subscribersList
        );
    }

    public static RouteMessage convertToProto(KvetkaModels.RouteModel args){
        var result = RouteMessage.newBuilder().setId(args.getId())
                .setNumRoute(args.getNumberRoute())
                .setName(args.getName())
                .setDistanceOwn(args.getDistance())
                .setTime(convertToTimestamp(args.getTime()));

        var tempList = new ArrayList<RoutePointMessage>();
        for (var t : args.getListPoints())
        {
            tempList.add(convertToProto(t));
        }

        result.addAllListPoints(tempList);
        return result.build();
    }


    public static KvetkaModels.RoutePointModel convertToTableModels(RoutePointMessage args){
        return new KvetkaModels.RoutePointModel(args.getId(), args.getRouteId(), (short)args.getNumPoint(), convertToTableModels(args.getCoordinates()), args.getSegmentLength());
    }

    public static RoutePointMessage convertToProto(KvetkaModels.RoutePointModel args){
        return RoutePointMessage.newBuilder().setId(args.getId())
                .setRouteId(args.getRouteId())
                .setNumPoint(args.getNumberPoint())
                .setCoordinates(convertToProto(args.getCoordinates()))
                .setSegmentLength(args.getSegmentLength()).build();
    }


    public static KvetkaModels.FHSSExcludedModel convertToTableModels(DependentFHSSMessage args){
        return new KvetkaModels.FHSSExcludedModel(args.getId(),
                args.getFrequencyKhz(),
                args.getDeviation(),
                args.getFhssId());
    }

    public static DependentFHSSMessage convertToProto(KvetkaModels.FHSSExcludedModel args){
        return DependentFHSSMessage.newBuilder().setId(args.getId())
                .setFrequencyKhz(args.getFrequency())
                .setDeviation(args.getDeviation())
                .setFhssId(args.getFhssId()).build();
    }



    public static KvetkaModels.AbstractCommonTable convertToTableModels(com.google.protobuf.Any args, NameTable nameTable){
        try {
            switch (nameTable){
                case TableJammer:
                    return convertToTableModels(args.unpack(JammerMessage.class));
                case TableFreqForbidden:
                    return convertToTableModels(args.unpack(FreqRangeMessage.class)).toFreqsForbiddenModel();
                case TableFreqImportant:
                    return convertToTableModels(args.unpack(FreqRangeMessage.class)).toFreqsImportantModel();
                case TableFreqKnown:
                    return convertToTableModels(args.unpack(FreqRangeMessage.class)).toFreqsKnownModel();
                case TableFreqRangesElint:
                    return convertToTableModels(args.unpack(FreqRangeMessage.class)).toRangesRIModel();
                case TableFreqRangesJamming:
                    return convertToTableModels(args.unpack(FreqRangeMessage.class)).toRangesRSModel();
                case TableSectorsElint:
                    return convertToTableModels(args.unpack(SectorMessage.class));
                case TableADSB:
                    return convertToTableModels(args.unpack(AirplaneMessage.class));
                case TableGlobalProperties:
                    return convertToTableModels(args.unpack(GlobalSettingsMessage.class));
                case TableResFFJam:
                    return convertToTableModels(args.unpack(ResFFJamMessage.class));
                case TableResFHSSJam:
                    return convertToTableModels(args.unpack(ResFHSSJamMessage.class));
                case TempSuppressFF:
                    return convertToTableModels(args.unpack(TempSuppressMessage.class)).toTempSuppressFWSModel();
                case TempSuppressFHSS:
                    return convertToTableModels(args.unpack(TempSuppressMessage.class)).toTempSuppressFHSSModel();
                case TableResFHSS:
                    return convertToTableModels(args.unpack(ResFHSSMessage.class));
                case TableSubscriberResFHSS:
                    return convertToTableModels(args.unpack(AbonentResFHSSMessage.class));
                case TableTrackSubscriberResFHSS:
                    return convertToTableModels(args.unpack(TrackAbonentResFHSSMessage.class));
                case TableResFF:
                    return convertToTableModels(args.unpack(ResFFMessage.class));
                case TableDigitalResFF:
                    return convertToTableModels(args.unpack(DigitalResFFMessage.class));
                case TableAnalogResFF:
                    return convertToTableModels(args.unpack(AnalogResFFMessage.class));
                case TableResFFDistribution:
                    return convertToTableModels(args.unpack(ResFFDistributMessage.class));
                case TableRoute:
                    return convertToTableModels(args.unpack(RouteMessage.class));
                case TableRoutePoint:
                    return convertToTableModels(args.unpack(RoutePointMessage.class));
                case TableFHSSExcludedJam:
                    return convertToTableModels(args.unpack(DependentFHSSMessage.class)).toSuppressFhssExcluded();
                default:
                    return null;
            }
        }catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static com.google.protobuf.Any convertToProto(KvetkaModels.AbstractCommonTable args, NameTable nameTable){
        switch (nameTable){
            case TableJammer:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.StationsModel)args));
            case TableFreqForbidden:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.FreqsForbiddenModel)args));
            case TableFreqImportant:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.FreqsImportantModel)args));
            case TableFreqKnown:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.FreqsKnownModel)args));
            case TableFreqRangesElint:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.RangesRIModel)args));
            case TableFreqRangesJamming:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.RangesRSModel)args));
            case TableSectorsElint:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.SectorsModel)args));
            case TableADSB:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.AirplanesModel)args));
            case TableGlobalProperties:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.GlobalPropertiesModel)args));
            case TableResFFJam:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.SuppressFWSModel)args));
            case TableResFHSSJam:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.SuppressFHSSModel)args));
            case TempSuppressFF:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.TempSuppressFWSModel)args));
            case TempSuppressFHSS:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.TempSuppressFHSSModel)args));
            case TableResFHSS:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.FHSSModel)args));
            case TableSubscriberResFHSS:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.SubscriberFHSSModel)args));
            case TableTrackSubscriberResFHSS:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.TrackSubscriberFHSSModel)args));
            case TableResFF:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.ReconFWSModel)args));
            case TableDigitalResFF:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.DigitalReconFWSModel)args));
            case TableAnalogResFF:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.AnalogReconFWSModel)args));
            case TableResFFDistribution:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.ReconFWSDistribModel)args));
            case TableRoute:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.RouteModel)args));
            case TableRoutePoint:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.RoutePointModel)args));
            case TableFHSSExcludedJam:
                return com.google.protobuf.Any.pack(convertToProto((KvetkaModels.SuppressFHSSExcludedModel)args));
            default:
                return null;
        }
    }


    public static KvetkaModels.ClassDataCommon convertToTableModels(java.util.List<com.google.protobuf.Any> args, NameTable nameTable){
        var result = new ArrayList<AbstractCommonTable>();

        for (com.google.protobuf.Any t : args)
        {
            result.add(convertToTableModels(t, nameTable));
        }
        return ClassDataCommon.convertToListAbstractCommonTable(result);
    }

    public static java.util.List<com.google.protobuf.Any> convertToProto(ClassDataCommon args, NameTable nameTable){
        var result = new ArrayList<com.google.protobuf.Any>();

        for (AbstractCommonTable t : args.listRecords)
        {
            result.add(convertToProto(t, nameTable));
        }

//        Test.newBuilder().addAllPoint(points);
        return result; //TODO: проверить
    }
}
