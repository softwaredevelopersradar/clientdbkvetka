package ClientDB.publisher;

import KvetkaModels.AbstractCommonTable;
import ClientDB.interfaces.IUpdateRecordTableEventListener;

import java.util.HashSet;
import java.util.Set;

public class RecordEventsManager{
    private Set<IUpdateRecordTableEventListener> listeners = new HashSet();

    public void addListener(IUpdateRecordTableEventListener listener) {
        listeners.add(listener);
    }

    public void deleteListener(IUpdateRecordTableEventListener listener) {
        listeners.remove(listener);
    }

    public void addRecordBroadcast(AbstractCommonTable rec){
        for(var listener:listeners){
            listener.onAddRecord(rec);
        }
    }

    public void changeRecordBroadcast(AbstractCommonTable rec){
        for(var listener:listeners){
            listener.onChangeRecord(rec);
        }
    }

    public void deleteRecordBroadcast(AbstractCommonTable rec){
        for(var listener:listeners){
            listener.onDeleteRecord(rec);
        }
    }
}
