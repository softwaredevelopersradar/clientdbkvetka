package ClientDB.publisher;

import KvetkaModels.AbstractCommonTable;
import ClientDB.eventargs.TableEventArgs;
import ClientDB.interfaces.IUpdateDataTableEventsListener;

import java.util.Set;
import java.util.HashSet;


public class TableEventsManager<T extends  AbstractCommonTable> {

    private Set<IUpdateDataTableEventsListener> listeners = new HashSet();

    public void addListener(IUpdateDataTableEventsListener listener) {
        listeners.add(listener);
    }

    public void deleteListener(IUpdateDataTableEventsListener listener) {
        listeners.remove(listener);
    }

    public void updateTableBroadcast(TableEventArgs<T> data){
        for(var listener:listeners){
            listener.onUpdateTable(data);
        }
    }
}
