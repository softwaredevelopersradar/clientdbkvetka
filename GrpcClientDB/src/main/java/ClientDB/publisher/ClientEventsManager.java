package ClientDB.publisher;

import ClientDB.eventargs.ConnectionEventArgs;
import ClientDB.interfaces.IClientEventsListener;

import java.util.HashSet;
import java.util.Set;

public class ClientEventsManager {
    private Set<IClientEventsListener> listeners = new HashSet();

    public void addListener(IClientEventsListener listener) {
        listeners.add(listener);
    }

    public void deleteListener(IClientEventsListener listener) {
        listeners.remove(listener);
    }

    public void connectBroadcast(ConnectionEventArgs args){
        for(IClientEventsListener listener:listeners){
            listener.onConnect(args);
        }
    }

    public void disconnectBroadcast(ConnectionEventArgs args){
        for(IClientEventsListener listener:listeners){
            listener.onDisconnect(args);
        }
    }

//    public void errorDBBroadcast(ErrorEventArgs rec){
//        for(IClientEventsListener listener:listeners){
//            listener.onErrorDataBase(rec);
//        }
//    }
}
