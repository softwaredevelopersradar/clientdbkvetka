package ClientDB;
import io.grpc.StatusRuntimeException;
import KvetkaModels.*;
import ClientDB.eventargs.*;
import ClientDB.exceptions.EnumClientError;
import ClientDB.exceptions.ExceptionClient;
import ClientDB.grpc_service.*;
import ClientDB.interfaces.IClassTables;
import ClientDB.publisher.*;
import io.grpc.Metadata;

import java.util.ArrayList;

public class  ClassTableT<T extends AbstractCommonTable> extends ClassTable implements IClassTables {
    protected NameTable name;
    protected Class<T> clazz;

    public TableEventsManager<T> tableEvents = new TableEventsManager<T>();
    public RecordEventsManager recordsEvents = new RecordEventsManager();
    
    public Event<TableEventArgs<T>> onUpdatedTable = new Event<TableEventArgs<T>>();
    public Event<AbstractCommonTable> onAddRecord = new Event<AbstractCommonTable>();
    public Event<AbstractCommonTable> onChangeRecord = new Event<AbstractCommonTable>();
    public Event<AbstractCommonTable> onDeleteRecord = new Event<AbstractCommonTable>();

    public ClassTableT(Class<T> clazz) {
        super();

        this.clazz = clazz;
        name = clazz.getAnnotation(InfoTable.class).name();
    }


    public void clickUpTable(ClassDataCommon data){
        tableEvents.updateTableBroadcast(new TableEventArgs<T>(data, clazz));
        onUpdatedTable.broadcast(new TableEventArgs<T>(data, clazz));
    }


    void clickUpRecord(RecordEventArgs data){
        var record = data.getAbstractRecord();
        switch(data.getNameAction()){
            case Add:
                onAddRecord.broadcast(record);
//                recordsEvents.addRecordBroadcast(data.getAbstractRecord()); //TODO: (T)data.getAbstractRecord() может надо сделать приведение типов?
                break;
            case Change:
                onChangeRecord.broadcast(record);
//                recordsEvents.changeRecordBroadcast(data.getAbstractRecord());
                break;
            case Delete:
                onDeleteRecord.broadcast(record);
//                recordsEvents.deleteRecordBroadcast(data.getAbstractRecord());
                break;
            default:
                break;

        }
    }






    protected boolean validConnection() {
        if (clientServiceDB == null) {
            //добавить ф-цию на стороне сервера Ping()
            // ErrorCallbackClient(this, Errors.EnumClientError.NoConnection, "");
            return false;
        }
        return true;
    }


    private Metadata createMetadata()
    {
        var result = new Metadata();
        Metadata.Key<String> namefield = Metadata.Key.of("clientid",Metadata.ASCII_STRING_MARSHALLER);
        result.put(namefield, String.valueOf(getId()));
        //result.put(namefield, getId();
        return result;
    }


    @Override
    public void add(Object obj) throws ExceptionClient {
        try {
            if (!validConnection()) {
                throw new ExceptionClient(EnumClientError.NoConnection);
            }
            AbstractCommonTable record = (AbstractCommonTable) obj;
            var message = ChangeRecordMessage.newBuilder()
                    .setTable(TypesConverter.convertToProto(name))
                    .setOperation(ChangeRecordOperation.ADD)
                    .setRecord(TypesConverter.convertToProto(record, name)).build();
//            MetadataUtils.attachHeaders(clientServiceDB.blockingStub, createMetadata()); //TODO: научиться передавать данные в Metadata
//            Metadata.Key<String> namefield = Metadata.Key.of("clientid",Metadata.ASCII_STRING_MARSHALLER);
//            var temp = clientServiceDB.blockingStub.getCallOptions();
            var result = clientServiceDB.blockingStub.changeRecord(message);
            System.out.println("Operation Add result: " + result.getIsSucceeded());
//            Context.current().withValue(Metadata.Key<String>("clientid"), String.valueOf(getId()) );
           //var result = clientServiceDB.blockingStub.withCallCredentials(CallCredentials.MetadataApplier().apply(createMetadata())).changeRecord(message, createMetadata());
        } catch (StatusRuntimeException except) {
            throw new ExceptionClient( (new ErrorEventArgs(name, NameTableOperation.Add, except.getMessage())).getMessage());
        }
        catch (ExceptionClient except) {
            //throw new ExceptionClient(except); //TODO: переделать вызов исключений
        } catch (Exception error) {
            try {
                throw new ExceptionClient(error.getMessage());
            } catch (ExceptionClient exceptionClient) {
                exceptionClient.printStackTrace();
            }
        }
    }



//public virtual async Task AddAsync(object obj)
//        {
//        try
//        {
//        if(!ValidConnection())
//        {
//        throw new ExceptionClient(Errors.EnumClientError.NoConnection);
//        }
//        AbstractCommonTable record=obj as AbstractCommonTable;
//
//        var message=new ChangeRecordMessage(){Table=Name.ConvertToProto(),Operation=ChangeRecordOperation.Add,Record=record.ConvertToProto(Name)};
//        var result=await ClientServiceDB.ChangeRecordAsync(message,CreateMetadata());
//        return;
//        }
//        catch(ExceptionClient except)
//        {
//        throw new ExceptionClient(except);
//        }
//        catch(Exception error)
//        {
//        throw new ExceptionClient(error.Message);
//        }
//        }
//

    @Override
    public void addRange(Object rangeObj)
    {
        try{
            if (!validConnection()) {
                throw new ExceptionClient(EnumClientError.NoConnection);
            }

            if(!(rangeObj instanceof ArrayList<?>))
            {
                throw new ExceptionClient("AddRange: Mismatch with expected type!");
            }

            var data=ClassDataCommon.convertToListAbstractCommonTable((ArrayList<T>)rangeObj);
            var message= ChangeRangeMessage.newBuilder()
                    .setTable(TypesConverter.convertToProto(name))
                    .setOperation(ChangeRecordOperation.ADD)
                    .addAllRecords(TypesConverter.convertToProto(data, name)).build();

            var result=clientServiceDB.blockingStub.changeRange(message);
        }
        catch(ExceptionClient except)
        {
            //throw new ExceptionClient(except); //TODO: переделать вызов исключений
        }
        catch(Exception error)
        {
            try {
                throw new ExceptionClient(error.getMessage());
            } catch (ExceptionClient exceptionClient) {
                exceptionClient.printStackTrace();
            }
        }
    }

//public virtual async Task AddRangeAsync(object rangeObj)
//        {
//        try
//        {
//        if(!ValidConnection())
//        {
//        throw new ExceptionClient(Errors.EnumClientError.NoConnection);
//        }
//        if(rangeObj is List<T>)
//        {
//        ClassDataCommon data=ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>);
//        var message=new ChangeRangeMessage(){Table=Name.ConvertToProto(),Operation=ChangeRecordOperation.Add};
//        message.Records.AddRange(data.ConvertToProto(Name));
//        var result=await ClientServiceDB.ChangeRangeAsync(message,CreateMetadata());
//        return;
//        }
//        throw new ExceptionClient("AddRange: Mismatch with expected type!");
//        }
//        catch(ExceptionClient except)
//        {
//        throw new ExceptionClient(except);
//        }
//        catch(Exception error)
//        {
//        throw new ExceptionClient(error.Message);
//        }
//        }

    @Override
    public void delete(Object obj){
        try{
            if (!validConnection()) {
                throw new ExceptionClient(EnumClientError.NoConnection);
            }

            AbstractCommonTable record = (AbstractCommonTable)obj;
            var message = ChangeRecordMessage.newBuilder()
                    .setTable(TypesConverter.convertToProto(name))
                    .setOperation(ChangeRecordOperation.DELETE)
                    .setRecord(TypesConverter.convertToProto(record, name)).build();
            var result = clientServiceDB.blockingStub.changeRecord(message);

        }
        catch(ExceptionClient except){
//            throw new ExceptionClient(except); //TODO: переделать вызов исключений
        }
        catch(Exception error){
            try {
                throw new ExceptionClient(error.getMessage());
            } catch (ExceptionClient exceptionClient) {
                exceptionClient.printStackTrace();
            }
        }
    }

    //public virtual async Task DeleteAsync(object obj)
//        {
//        try
//        {
//        if(!ValidConnection())
//        {
//        throw new ExceptionClient(Errors.EnumClientError.NoConnection);
//        }
//        var record=obj as AbstractCommonTable;
//        var message=new ChangeRecordMessage(){Table=Name.ConvertToProto(),Operation=ChangeRecordOperation.Delete,Record=record.ConvertToProto(Name)};
//        var result=await ClientServiceDB.ChangeRecordAsync(message,CreateMetadata());
//        return;
//        }
//        catch(ExceptionClient except)
//        {
//        throw new ExceptionClient(except);
//        }
//        catch(Exception error)
//        {
//        throw new ExceptionClient(error.Message);
//        }
//        }

    @Override
    public void removeRange(Object rangeObj){
        try{
            if (!validConnection()) {
                throw new ExceptionClient(EnumClientError.NoConnection);
            }

            if(!(rangeObj instanceof ArrayList<?>))
            {
                throw new ExceptionClient("RemoveRange: Mismatch with expected type!");
            }

            var data=ClassDataCommon.convertToListAbstractCommonTable((ArrayList<T>)rangeObj);
            var message= ChangeRangeMessage.newBuilder()
                    .setTable(TypesConverter.convertToProto(name))
                    .setOperation(ChangeRecordOperation.DELETE)
                    .addAllRecords(TypesConverter.convertToProto(data, name)).build();

            var result=clientServiceDB.blockingStub.changeRange(message);
        }
        catch(ExceptionClient except){
//            throw new ExceptionClient(except); //TODO: переделать вызов исключений
        }
        catch(Exception error){
            try {
                throw new ExceptionClient(error.getMessage());
            } catch (ExceptionClient exceptionClient) {
                exceptionClient.printStackTrace();
            }
        }
    }

//public virtual async Task RemoveRangeAsync(object rangeObj)
//        {
//        try
//        {
//        if(!ValidConnection())
//        {
//        throw new ExceptionClient(Errors.EnumClientError.NoConnection);
//        }
//        if(rangeObj is List<T>)
//        {
//        ClassDataCommon data=ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>);
//        var message=new ChangeRangeMessage(){Table=Name.ConvertToProto(),Operation=ChangeRecordOperation.Delete};
//        message.Records.AddRange(data.ConvertToProto(Name));
//        var result=await ClientServiceDB.ChangeRangeAsync(message,CreateMetadata());
//        return;
//        }
//        throw new ExceptionClient("RemoveRange: Mismatch with expected type!");
//        }
//        catch(ExceptionClient except)
//        {
//        throw new ExceptionClient(except);
//        }
//        catch(Exception error)
//        {
//        throw new ExceptionClient(error.Message);
//        }
//        }

    @Override
    public void change(Object obj){
        try{
            if (!validConnection()) {
                throw new ExceptionClient(EnumClientError.NoConnection);
            }

            AbstractCommonTable record = (AbstractCommonTable)obj;
            var message = ChangeRecordMessage.newBuilder()
                    .setTable(TypesConverter.convertToProto(name))
                    .setOperation(ChangeRecordOperation.CHANGE)
                    .setRecord(TypesConverter.convertToProto(record, name)).build();
            var result = clientServiceDB.blockingStub.changeRecord(message);

        }
        catch(ExceptionClient except){
//            throw new ExceptionClient(except); //TODO: переделать вызов исключений
        }
        catch(Exception error){
            try {
                throw new ExceptionClient(error.getMessage());
            } catch (ExceptionClient exceptionClient) {
                exceptionClient.printStackTrace();
            }
        }
    }

//public virtual async Task ChangeAsync(object obj)
//        {
//        try
//        {
//        if(!ValidConnection())
//        {
//        throw new ExceptionClient(Errors.EnumClientError.NoConnection);
//        }
//        AbstractCommonTable record=obj as AbstractCommonTable;
//        var message=new ChangeRecordMessage(){Table=Name.ConvertToProto(),Operation=ChangeRecordOperation.Change,Record=record.ConvertToProto(Name)};
//        var result=await ClientServiceDB.ChangeRecordAsync(message,CreateMetadata());
//        return;
//        }
//        catch(ExceptionClient except)
//        {
//        throw new ExceptionClient(except);
//        }
//        catch(Exception error)
//        {
//        throw new ExceptionClient(error.Message);
//        }
//        }

    @Override
    public void changeRange(Object rangeObj){
        try{
            if (!validConnection()) {
                throw new ExceptionClient(EnumClientError.NoConnection);
            }
            if(!(rangeObj instanceof ArrayList<?>))
            {
                throw new ExceptionClient("ChangeRange: Mismatch with expected type!");
            }

            var data=ClassDataCommon.convertToListAbstractCommonTable((ArrayList<T>)rangeObj);
            var message= ChangeRangeMessage.newBuilder()
                    .setTable(TypesConverter.convertToProto(name))
                    .setOperation(ChangeRecordOperation.CHANGE)
                    .addAllRecords(TypesConverter.convertToProto(data, name)).build();

            var result=clientServiceDB.blockingStub.changeRange(message);
        }
        catch(ExceptionClient except){
//            throw new ExceptionClient(except); //TODO: переделать вызов исключений
        }
        catch(Exception error){
            try {
                throw new ExceptionClient(error.getMessage());
            } catch (ExceptionClient exceptionClient) {
                exceptionClient.printStackTrace();
            }
        }
    }


//public virtual async Task ChangeRangeAsync(object rangeObj)
//        {
//        try
//        {
//        if(!ValidConnection())
//        {
//        throw new ExceptionClient(Errors.EnumClientError.NoConnection);
//        }
//        if(rangeObj is List<T>)
//        {
//        ClassDataCommon data=ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>);
//        var message=new ChangeRangeMessage(){Table=Name.ConvertToProto(),Operation=ChangeRecordOperation.Change};
//        message.Records.AddRange(data.ConvertToProto(Name));
//        var result=await ClientServiceDB.ChangeRangeAsync(message,CreateMetadata());
//        return;
//        }
//        throw new ExceptionClient("ChangeRange: Mismatch with expected type!");
//        }
//        catch(ExceptionClient except)
//        {
//        throw new ExceptionClient(except);
//        }
//        catch(Exception error)
//        {
//        throw new ExceptionClient(error.Message);
//        }
//        }

    @Override
    public void clear(){
        try{
            if (!validConnection()) {
                throw new ExceptionClient(EnumClientError.NoConnection);
            }

            var message= NameTableMessage.newBuilder()
                    .setTable(TypesConverter.convertToProto(name)).build();
            var result=clientServiceDB.blockingStub.clearTable(message);
        }
        catch(ExceptionClient except){
//            throw new ExceptionClient(except); //TODO: переделать вызов исключений
        }
        catch(Exception error){
            try {
                throw new ExceptionClient(error.getMessage());
            } catch (ExceptionClient exceptionClient) {
                exceptionClient.printStackTrace();
            }
        }
    }

//public virtual async Task CLearAsync()
//        {
//        try
//        {
//        if(!ValidConnection())
//        {
//        throw new ExceptionClient(Errors.EnumClientError.NoConnection);
//        }
//        var message=new NameTableMessage(){Table=Name.ConvertToProto()};
//        var result=await ClientServiceDB.ClearTableAsync(message,CreateMetadata());
//        return;
//        }
//        catch(ExceptionClient except)
//        {
//        throw new ExceptionClient(except);
//        }
//        catch(Exception error)
//        {
//        throw new ExceptionClient(error.Message);
//        }
//        }


    @Override
    public ArrayList<T> load(){
        try{
            if (!validConnection()) {
                throw new ExceptionClient(EnumClientError.NoConnection);
            }

            var message= NameTableMessage.newBuilder().setTable(TypesConverter.convertToProto(name)).build();
            var data=clientServiceDB.blockingStub.loadTable(message);
            var result = TypesConverter.convertToTableModels(data.getRecordsList(), TypesConverter.convertToTableModels(data.getTable()));
            return result.toList(this, clazz); //TODO: проверить this
        }
        catch(ExceptionClient except){
            return null;
//            throw new ExceptionClient(except); //TODO: переделать вызов исключений
        }
        catch(Exception error){
            try {
                throw new ExceptionClient(error.getMessage());
            } catch (ExceptionClient exceptionClient) {
                exceptionClient.printStackTrace();
            }
            return null;
        }
    }

//public virtual async Task<List<V>>LoadAsync<V>()where V:AbstractCommonTable
//        {
//        try
//        {
//        if(!ValidConnection())
//        {
//        throw new ExceptionClient(Errors.EnumClientError.NoConnection);
//        }
//        var message=new NameTableMessage(){Table=Name.ConvertToProto()};
//        var temp=await ClientServiceDB.LoadTableAsync(message,CreateMetadata());
//        var result=temp.Records.ConvertToDBModel(Name);
//        return result.ToList<V>();
//        }
//        catch(ExceptionClient except)
//        {
//        throw new ExceptionClient(except);
//        }
//        //catch (FaultException<InheritorsException.ExceptionWCF> except)
//        //{
//        //    throw new ExceptionDatabase(except.Detail);
//        //}
//        catch(Exception error)
//        {
//        throw new ExceptionClient(error.Message);
//        }
//        }
//
//


//    class AuthenticationCallCredentials extends CallCredentials {
//
//        private int id;
//
//        public AuthenticationCallCredentials(int id) {
//            this.id = id;
//        }
//
//        @Override
//        public void applyRequestMetadata(
//                RequestInfo requestInfo,
//                Executor executor,
//                MetadataApplier metadataApplier) {
//            executor.execute(() -> {
//                try {
//                    Metadata headers = new Metadata();
//                    headers.put("clientid", id.toString());
//                    metadataApplier.apply(headers);
//                } catch (Throwable e) {
//                    metadataApplier.fail(Status.UNAUTHENTICATED.withCause(e));
//                }
//            });
//
//        }
//
//        @Override
//        public void thisUsesUnstableApi() {
//            // yes this is unstable :(
//        }
//    }


}