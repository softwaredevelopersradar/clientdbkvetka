package ClientDB.interfaces;

import KvetkaModels.AbstractCommonTable;
import ClientDB.eventargs.TableEventArgs;

public interface IUpdateDataTableEventsListener {
    
    public void onUpdateTable(TableEventArgs<? extends  AbstractCommonTable> data);
}
