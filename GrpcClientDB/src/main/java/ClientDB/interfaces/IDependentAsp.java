package ClientDB.interfaces;

import KvetkaModels.AbstractDependentASP;

import java.util.ArrayList;

public interface IDependentAsp {
    <T extends AbstractDependentASP> ArrayList<T> loadByFilter(int numberASP);
    void clearByFilter(int numberASP);
}
