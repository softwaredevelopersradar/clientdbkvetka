package ClientDB.interfaces;

import KvetkaModels.AbstractCommonTable;

public interface IUpdateRecordTableEventListener {
    public void onAddRecord(AbstractCommonTable record);
    public void onChangeRecord(AbstractCommonTable record);
    public void onDeleteRecord(AbstractCommonTable record);
}
