package ClientDB.interfaces;

import ClientDB.eventargs.ConnectionEventArgs;

public interface IClientEventsListener {
    public void onConnect(ConnectionEventArgs args);
    public void onDisconnect(ConnectionEventArgs args);
//    public void onUpData(AbstractCommonTable record);
 //   public void onErrorDataBase(ErrorEventArgs args);
}
