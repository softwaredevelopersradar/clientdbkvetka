package ClientDB.interfaces;
import ClientDB.exceptions.ExceptionClient;
import ClientDB.publisher.TableEventsManager;
import KvetkaModels.*;

import java.util.ArrayList;

public interface IClassTables {
    TableEventsManager tableEvents = new TableEventsManager();

    void add(Object obj) throws ExceptionClient;
    void change(Object obj);
    void delete(Object obj);
    void addRange(Object rangeObj);
    void removeRange(Object rangeObj);
    void changeRange(Object rangeObj);
    void clear();
    <T extends AbstractCommonTable> ArrayList<T> load();

//    List<AbstractCommonTable> Load(AbstractCommonTable type);
   //void clickUpTable(ClassDataCommon data);
    void dispose() throws InterruptedException;
}
