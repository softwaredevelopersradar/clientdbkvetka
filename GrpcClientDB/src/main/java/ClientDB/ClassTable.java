package ClientDB;

import java.util.concurrent.TimeUnit;

public class ClassTable {
    public static GrpcClientModel clientServiceDB = null;

    private static int id = 0;

    public ClassTable(GrpcClientModel clientServiceDB, int id)
    {
        if (this.clientServiceDB != null)
            return;

        this.clientServiceDB = clientServiceDB;
        this.id = id;
    }

    public ClassTable()
    { }

    public int getId(){
        return this.id;
    }
    static void setId(int id){
        id = id;
    }

    public void dispose() throws InterruptedException {
        if (clientServiceDB == null)
            return;

        clientServiceDB.channel.shutdownNow().awaitTermination(1, TimeUnit.SECONDS);

        //ClientServiceDB.Abort();
        clientServiceDB = null;
    }
}
