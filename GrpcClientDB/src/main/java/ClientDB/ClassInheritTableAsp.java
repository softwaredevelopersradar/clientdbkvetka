package ClientDB;

import ClientDB.exceptions.EnumClientError;
import ClientDB.exceptions.ExceptionClient;
import ClientDB.grpc_service.AspFilterMessage;
import ClientDB.interfaces.IDependentAsp;
import KvetkaModels.*;

import java.util.ArrayList;

public class ClassInheritTableAsp<T extends AbstractDependentASP> extends ClassTableT<T> implements IDependentAsp {
    public ClassInheritTableAsp(Class<T> clazz)
    {
        super(clazz);
    }

    @Override
    public ArrayList<T> loadByFilter(int numberASP){
        try{
            if (!validConnection()) {
                throw new ExceptionClient(EnumClientError.NoConnection);
            }
            var message= AspFilterMessage.newBuilder().setTable(TypesConverter.convertToProto(name)).setNumberASP(numberASP).build();
            var data = clientServiceDB.blockingStub.loadByFilterAsp(message);
            var result = TypesConverter.convertToTableModels(data.getRecordsList(), TypesConverter.convertToTableModels(data.getTable()));
            return result.toList(this, clazz); //TODO: проверить this
        }
        catch (ExceptionClient except)
        {
            return  null; //TODO: переделать вызов исключений
//            throw new ExceptionClient(except);
        }
        catch (Exception error)
        {
            try {
                throw new ExceptionClient(error.getMessage());
            } catch (ExceptionClient exceptionClient) {
                exceptionClient.printStackTrace();
            }
            return null;
        }
    }


    //public async Task<List<V>> LoadByFilterAsync<V>(int NumberASP) where V : AbstractDependentASP
    //{
    //    //try
    //    //{
    //    //    if (!ValidConnection())
    //    //    {
    //    //        throw new ExceptionClient(Errors.EnumClientError.NoConnection);
    //    //    }
    //    //    var result = await ClientServiceDB.LoadDataFilterASPAsync(Name, NumberASP, Id);
    //    //    return result.ToList<V>();
    //    //}
    //    //catch (ExceptionClient except)
    //    //{
    //    //    throw new ExceptionClient(except);
    //    //}
    //    //catch (FaultException<InheritorsException.ExceptionWCF> except)
    //    //{
    //    //    throw new ExceptionDatabase(except.Detail);
    //    //}
    //    //catch (Exception error)
    //    //{
    //    //    throw new ExceptionClient(error.Message);
    //    //}
    //}

    @Override
    public void clearByFilter(int numberASP){
        try{
            if (!validConnection()) {
                throw new ExceptionClient(EnumClientError.NoConnection);
            }

            var message= AspFilterMessage.newBuilder().setTable(TypesConverter.convertToProto(name)).setNumberASP(numberASP).build();
            var data = clientServiceDB.blockingStub.clearByFilterAsp(message);
            return;
        }
        catch (ExceptionClient except)
        {
            //throw new ExceptionClient(except);
        }
        catch (Exception error)
        {
            try {
                throw new ExceptionClient(error.getMessage());
            } catch (ExceptionClient exceptionClient) {
                exceptionClient.printStackTrace();
            }
        }
    }
}
