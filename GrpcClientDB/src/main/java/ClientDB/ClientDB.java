package ClientDB;
import KvetkaModels.*;
import ClientDB.eventargs.ConnectionEventArgs;
import ClientDB.publisher.ClientEventsManager;
import io.grpc.StatusRuntimeException;

import java.util.Map;
import java.util.concurrent.*;
import java.util.logging.Logger;

import ClientDB.grpc_service.*;
import io.grpc.stub.StreamObserver;


public class ClientDB {

    private int id;
    private String name = "";
    private String serverIp = "127.0.0.1";
    private int serverPort = 30051;

    private static final Logger logger = Logger.getLogger(ClientDB.class.getName());

    private GrpcClientModel clientGrpc;

    public ClientEventsManager clientEvents = new ClientEventsManager();

    /** Construct client for accessing DB server using IP and Port.
     */
    public ClientDB(String name, String serverIp, int serverPort) {
        try{
            this.name = name;
            this.serverIp = serverIp;
            this.serverPort = serverPort;
        }
        catch (Exception ex){ }
    }



    /** словарь, который хранит NameTable - имя таблицы,
     //     ITableAction - объект класса, который реализует интерфейс(ITableAction - действия над таблицей)
     //     */
    public final Map<NameTable, ? extends  ClassTableT> tables = Map.ofEntries(
            Map.entry(NameTable.TableJammer, new ClassTableT<StationsModel>(StationsModel.class)),
            Map.entry( NameTable.TableFreqForbidden, new ClassInheritTableAsp(FreqsForbiddenModel.class)),
            Map.entry( NameTable.TableFreqKnown, new ClassInheritTableAsp(FreqsKnownModel.class)),
            Map.entry( NameTable.TableFreqImportant, new ClassInheritTableAsp(FreqsImportantModel.class)),
            Map.entry( NameTable.TableFreqRangesElint, new ClassInheritTableAsp(RangesRIModel.class)),
            Map.entry( NameTable.TableFreqRangesJamming, new ClassInheritTableAsp(RangesRSModel.class)),
            Map.entry( NameTable.TableSectorsElint, new ClassInheritTableAsp(SectorsModel.class)),
            Map.entry( NameTable.TableADSB, new ClassTableT<AirplanesModel>(AirplanesModel.class)),
            Map.entry( NameTable.TableGlobalProperties, new ClassTableT<GlobalPropertiesModel>(GlobalPropertiesModel.class)),
            Map.entry( NameTable.TableResFFJam, new ClassInheritTableAsp<SuppressFWSModel>(SuppressFWSModel.class)),
            Map.entry( NameTable.TableResFHSSJam, new ClassInheritTableAsp<SuppressFHSSModel>(SuppressFHSSModel.class)),
            Map.entry( NameTable.TempSuppressFF, new ClassInheritTableAsp<TempSuppressFWSModel>(TempSuppressFWSModel.class)),
            Map.entry( NameTable.TempSuppressFHSS, new ClassInheritTableAsp<TempSuppressFHSSModel>(TempSuppressFHSSModel.class)),
            Map.entry( NameTable.TableResFHSS, new ClassTableT<FHSSModel>(FHSSModel.class)),
            Map.entry( NameTable.TableSubscriberResFHSS, new ClassTableT<SubscriberFHSSModel>(SubscriberFHSSModel.class)),
            Map.entry( NameTable.TableTrackSubscriberResFHSS, new ClassTableT<TrackSubscriberFHSSModel>(TrackSubscriberFHSSModel.class)),
            Map.entry( NameTable.TableResFF, new ClassTableT<ReconFWSModel>(ReconFWSModel.class)),
            Map.entry( NameTable.TableDigitalResFF, new ClassTableT<DigitalReconFWSModel>(DigitalReconFWSModel.class)),
            Map.entry( NameTable.TableAnalogResFF, new ClassTableT<AnalogReconFWSModel>(AnalogReconFWSModel.class)),
            Map.entry( NameTable.TableResFFDistribution, new ClassTableT<ReconFWSDistribModel>(ReconFWSDistribModel.class)),
            Map.entry( NameTable.TableRoute, new ClassTableT<RouteModel>(RouteModel.class)),
            Map.entry( NameTable.TableRoutePoint, new ClassTableT<RoutePointModel>(RoutePointModel.class)),
            Map.entry( NameTable.TableFHSSExcludedJam, new ClassTableT<SuppressFHSSExcludedModel>(SuppressFHSSExcludedModel.class))
    );


    public void Abort() throws Exception {
        for(var table : tables.values()) {
            table.dispose();
        }

        clientGrpc.channel.shutdownNow().awaitTermination(5,TimeUnit.SECONDS);
        clientGrpc = null;
        clientEvents.disconnectBroadcast(new ConnectionEventArgs(ConnectionEventArgs.actServer.Disconnect));
    }

    /** Say hello to server. */
    public void ping(String name) {
//        logger.info("Will try to greet " + name + " ...");
        IdMessage request = IdMessage.newBuilder().setId(1).build();
        PingResponse response;
        try {
            response = clientGrpc.blockingStub.ping(request);
        } catch (StatusRuntimeException e) {
//            logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
            return;
        }
//        logger.info("Greeting: " + response.getIsFirstPingRequest());
    }

    public boolean isConnected()
    {
        if (clientGrpc == null)
            return false;
        try
        {
            return clientGrpc.blockingStub.ping(IdMessage.newBuilder().setId(id).build()).getIsFirstPingRequest();
        }
        catch (Exception except)
        {
            //OnErrorDataBase(this, new OperationTableEventArgs(NameTableOperation.Update, except.Message));
            return false;
        }
    }

    public void connect()
    {
        try
        {
            if (isConnected())
            {
                clientEvents.connectBroadcast(new ConnectionEventArgs(ConnectionEventArgs.actServer.Connect));
                return;
            }

            clientGrpc = new GrpcClientModel(serverIp, serverPort);
            CompletableFuture.runAsync(() -> {
                try {
                    subscribe();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });

            id = clientGrpc.blockingStub.connect(PingRequest.newBuilder().setClientName(this.name).build()).getId();

            ClassTable.setId(id);
            ClassTable.clientServiceDB = clientGrpc;
            clientEvents.connectBroadcast(new ConnectionEventArgs(ConnectionEventArgs.actServer.Connect, "Id =" + id));
        }
        catch (Exception error)
        {
            clientEvents.disconnectBroadcast(new ConnectionEventArgs(ConnectionEventArgs.actServer.Disconnect, error.getMessage()));
        }
    }

    public void disconnect() throws Exception {
        try
        {
            if (clientGrpc != null)
            {
                clientGrpc.blockingStub.disconnect(IdMessage.newBuilder().setId(this.id).build());
                Abort();
            }
        }
        catch (Exception error)
        {
            Abort();
            //throw new ExceptionClient(error.getMessage()); //TODO: переделать вызов исключений
        }
    }

    private void subscribe() throws InterruptedException {
        clientGrpc.asyncStub.subscribe( DefaultRequest.newBuilder().build(),new StreamObserver<LoadMessage>(){
            @Override
            public void onNext(LoadMessage response) {
                System.out.println("Got something from observer...");
//                Platform.runLater(() -> {
                    var nameTable = TypesConverter.convertToTableModels(response.getTable());
                    var records = TypesConverter.convertToTableModels(response.getRecordsList(), nameTable);

                    //OnUpData(this, new DataEventArgs(nameTable, records));
                    tables.get(nameTable).clickUpTable(records);

//                });
            }

            @Override
            public void onError(Throwable throwable) {
                try {
                    disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCompleted() {
                var t = 5;
            }


        });
    }
}
