package ClientDB;

import ClientDB.grpc_service.TransmissionGrpc;
import ClientDB.grpc_service.TransmissionGrpc.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

class GrpcClientModel {
    public static TransmissionBlockingStub blockingStub;
    public static TransmissionStub asyncStub;
    public static ManagedChannel channel;

    GrpcClientModel(String serverIp, int serverPort)
    {
        channel = ManagedChannelBuilder.forAddress(serverIp, serverPort).usePlaintext().build();
        blockingStub = TransmissionGrpc.newBlockingStub(channel);
        asyncStub = TransmissionGrpc.newStub(channel);
    }
}
